from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/users')
    def index():
        return 'Hello From users service !!USERS!!'

    @app.route('/users/test')
    def users_test():
        return 'This is USERS-service'

    return app
