from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/json')
    def index():
        return 'Hello From json service !!JSON!!'

    @app.route('/json/json')
    def json():
        return 'This is JSON-service'

    @app.route('/json/test')
    def json_test():
        return 'This is JSON-service'

    return app
