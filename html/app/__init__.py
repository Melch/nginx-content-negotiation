from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/')
    def index():
        return 'Hello From html service !!HTML!!'

    @app.route('/json')
    def json():
        return 'This is the HTML service serving json'

    @app.route('/users')
    def users():
        return 'This is the HTML service serving users'

    return app
